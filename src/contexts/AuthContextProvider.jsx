import { useState, useEffect } from "react";
import { AuthContext } from "./AuthContext";
import Cookies from "js-cookie";
import { jwtDecode } from "jwt-decode";

export default function AuthContextProvider({children}) {
    
    const [user, setUser] = useState({
        nome : "",
        cognome : "",
        email : "",
        password : "",
        ruoli : ""
    })
    
    useEffect(() =>{
        if(Cookies.get("Token") != undefined) {
            setUser(jwtDecode(JSON.parse(Cookies.get("Token")).token))
        }
        }, [])

    return (
        <>
            <AuthContext.Provider value={[user, setUser]}>
                {children}
            </AuthContext.Provider>
        </>
    )
}