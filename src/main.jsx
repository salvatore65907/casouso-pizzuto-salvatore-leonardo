import React from 'react'
import ReactDOM from 'react-dom/client'
import 'bootstrap/dist/js/bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import './index.css'

import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import AuthContextProvider from './contexts/AuthContextProvider.jsx'

import { Provider } from 'react-redux'
import store, { persistStorage } from './Store/index.jsx'
import { PersistGate } from 'redux-persist/lib/integration/react'

import Layout from './pages/Layout.jsx'
import Home from './pages/Home.jsx'
import Login from './pages/Login.jsx'
import Registration from './pages/Registration.jsx'
import AllUsers from './pages/AllUsers.jsx'

const router = createBrowserRouter([
  {
    element : <AuthContextProvider><Layout /></AuthContextProvider>,
    children : [
      {
        path : "/",
        children : [
          {
            path : "",
            element : <Home />
          },
          {
            path : "login",
            element : <Login />
          },
          {
            path : "registration",
            element : <Registration />
          },
          {
            path : "users",
            element : <AllUsers />
          }
        ]
      }
    ]
  }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistStorage}>
        <RouterProvider router={router}/>
    </PersistGate>
 </Provider>
)
