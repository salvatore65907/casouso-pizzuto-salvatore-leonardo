import css from './Footer.module.css'

export default function Footer() {
    return (
        <footer className={"p-4 text-center position-relative mt-3 " + css.myFooter}>
            <div className="container">
                <p><i className="bi bi-envelope-at-fill"></i> : salvatore.leonardo.pizzuto00@gmail.com</p>
                <p><i className="bi bi-phone-fill"></i> : +39 333 3333333</p>
                <p>Copyright &copy; 2024 Salvatore Leonardo Pizzuto </p>
            </div>
        </footer>
    )
}