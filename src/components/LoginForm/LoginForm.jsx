import { useState, useContext } from 'react'
import { jwtDecode } from 'jwt-decode'
import Cookies from 'js-cookie'
import { AuthContext } from '../../contexts/AuthContext'
import css from './LoginForm.module.css'
import { useSelector, useDispatch } from "react-redux"
import { setToken } from "../../Store/Reducers/Login"
import { useNavigate } from 'react-router-dom'


export default function LoginForm() {
    
    const [user, setUser] = useContext(AuthContext)

    const [formData, setFormData] = useState({
        email : "",
        password : ""
    })

    const navigateTo = useNavigate()

    /*let token = useSelector((state) => {
        //console.log(state.login.token)
        return state.login.token
    })*/

    let dispatch = useDispatch()

    const fetchData = async (formData) => {
        //Prendo un oggetto e lo trasformo in JSON (tutto diventa stringa)
        const jsonData = JSON.stringify(formData)
        //La fetch è asincrona e restituisce una promise: promessa completamento richiesta. Ha tre stati (?, fulfilled, rejected)
        const response = await fetch("http://localhost:8080/api/utente/login", {
          mode : "cors",
          method : "POST",
          headers: {
            "Content-Type" : "application/json"
          },
          body : jsonData
        })

        return response
    }

    const handleChange = (e) => {
        const {name, value} = e.target
        setFormData({...formData, [name] : value})
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const result = await fetchData(formData)
        if(result.status == 200) {
            const token = await result.json()
            const jwtString = JSON.stringify(token)
            //Cookies.set("Token", jwtString, {expires : 31})
            const decoded = jwtDecode(jwtString)
            console.log(decoded)
            dispatch(setToken(jwtString))
            //setUser(decoded)
            navigateTo("/")

        } else {
            setFormData({...formData, "password" : ""})
            alert("Error")
        }
    }
    
    return (
        <div className={css.myLogin}>
            <h1>Welcome!</h1>
            <div className="container mt-5">
                <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                        <label htmlFor="email" className="form-label">Your email</label>
                        <input type="email" name="email" value={formData.email} className="form-control" id="email" aria-describedby="emailHelp" 
                        onChange={handleChange}/>
                        {/*<div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>*/}
                    </div>
                    <div className="mb-3">
                        <label htmlFor="password" className="form-label">Your password</label>
                        <input type="password" name="password" value={formData.password} className="form-control    " id="password" onChange={handleChange} />
                    </div>
                    <button type="submit" className="btn btn-success">Login</button>
                </form>
            </div>
        </div>
    )
}