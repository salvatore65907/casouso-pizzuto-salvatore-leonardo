import { useEffect } from "react"
import { useState } from "react"
import css from './ListUsers.module.css'
import { useSelector } from "react-redux"

export default function ListUsers() {

    const [users, setUsers] = useState([])

    let login = useSelector((state) => {
        return state.login
    })

    const fetchData = async () => {
        //La fetch è asincrona e restituisce una promise: promessa completamento richiesta. Ha tre stati (?, fulfilled, rejected)
        const response = await fetch("http://localhost:8080/api/utente/getUtenti", {
            mode: "cors",
            method: "GET",
            headers:{
                'Authorization': `Bearer ` + JSON.parse(login.token).token
            }

        })


        let dbUsers = await response.json()
        //console.log(response.status)
        setUsers(
                dbUsers.map((user) => ({
                name: user.nome,
                surname: user.cognome,
                email: user.email,
                roles: user.ruoli.map((ruolo) => ruolo.tipologia).join(", ")
            }))
        )
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleEdit = () => {
        alert("Edit")
    }

    const handleDelete = () => {
        alert("Delete")
    }

    return (
        <>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Surname</th>
                        <th scope="col">Email</th>
                        <th scope="col">Roles</th>
                        <th scope="col">Options</th>
                    </tr>
                </thead>
                <tbody>
                    {
                       users.map((user, index) =>
                            <tr key={++index}>
                                <th scope="row">{++index}</th>
                                <td>{user.name}</td>
                                <td>{user.surname}</td>
                                <td>{user.email}</td>
                                <td>{user.roles ? user.roles : "None"}</td>
                                <td><i className={"bi bi-pencil-square " + css.hand} onClick={handleEdit}></i> <i className={"bi bi-trash " + css.hand} onClick={handleDelete}></i></td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
        </>
    )
}