import { useState } from "react"
import { useNavigate } from "react-router-dom"
import css from "./RegistrationForm.module.css"

export default function RegistrationForm() {

    const [formData, setFormData] = useState({
        nome : "",
        cognome : "",
        email : "",
        password : ""
    })

    const navigateTo = useNavigate()

    const fetchData = async (formData) => {
        //Prendo un oggetto e lo trasformo in JSON (tutto diventa stringa)
        //console.log(formData)
        const jsonData = JSON.stringify(formData)
        //console.log(jsonData)
        
        //La fetch è asincrona e restituisce una promise: promessa completamento richiesta. Ha tre stati (?, fulfilled, rejected)
        const response = await fetch("http://localhost:8080/api/utente/registrazione", {
          mode : "cors",
          method : "POST",
          headers: {
            "Content-Type" : "application/json"
          },
          body : jsonData
        })

        return response
    }

    const handleChange = (e) => {
        const {name, value} = e.target
        setFormData({...formData, [name] : value})
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        if(checkNameSurname(formData.nome) && checkNameSurname(formData.cognome) && checkEmail(formData.email) && checkPassword(formData.password)) {
            const result = await fetchData(formData)
            //console.log(result)
            if(result.status == 200) {
                /*const token = await result.json()
                const jwtString = JSON.stringify(token)
                //Cookies.set("Token", jwtString, {expires : 31})
                const decoded = jwtDecode(jwtString)
                setUser(decoded)*/
                alert("Welcome! You are now registered.")
                navigateTo("/login")
            } else {
                alert("Something went wrong. Please, try again.")
            }
        } else {
            alert("Values not allowed. Try again.")
        }
    }

    function checkNameSurname(text){
        let regex=/^[A-Za-zÀ-Ùà-ù][a-zà-ù]*$/;
    
        return regex.test(text);
        //alert("check nome "+regex.test(text));
    }
    
    function checkEmail(text){
        let regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,8}$/;
        return regex.test(text);
        //alert("check email "+regex.test(text));
    }
    
    function checkPassword(text){
        let regex=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@.#$!%*?&^])[A-Za-z\d@.#$!%*?&]{6,20}$/;
        return regex.test(text);
       // alert("check password "+regex.test(text));
    }
    

    return (
        <>
            <div className={css.myForm}>
                <h1>Join Us!</h1>
                <div className="container mt-5">
                    <form onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <label htmlFor="nome" className="form-label">Your name</label>
                            <input type="text" name="nome" value={formData.nome} className="form-control" id="nome" aria-describedby="nomeHelp"
                                onChange={handleChange} />
                         </div>
                        <div className="mb-3">
                            <label htmlFor="cognome" className="form-label">Your surname</label>
                            <input type="text" name="cognome" value={formData.cognome} className="form-control" id="cognome" aria-describedby="cognomeHelp"
                                onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="email" className="form-label">Insert your email</label>
                            <input type="email" name="email" value={formData.email} className="form-control" id="email" aria-describedby="emailHelp"
                                onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="password" className="form-label">Choose a password</label>
                            <input type="password" name="password" value={formData.password} className="form-control" id="password" onChange={handleChange} />
                        </div>
                        <button type="submit" className="btn btn-success">Register</button>
                    </form>
                </div>
            </div>
        </>
    )
}