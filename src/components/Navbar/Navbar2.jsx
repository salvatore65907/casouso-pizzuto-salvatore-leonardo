import { useState, useContext } from "react"
import css from "./Navbar.module.css"
import { AuthContext } from "../../contexts/AuthContext"
import { NavLink, useNavigate } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { resetToken } from "../../Store/Reducers/Login"

export default function Navbar2() {

    let login = useSelector((state) => {
        //console.log(state.login.token)
        return state.login
    })

    let dispatch = useDispatch()

    const navigateTo = useNavigate()


    //let dispatch = useDispatch()

    function isAdmin() {
        //console.log(token.ruoli.includes("Admin"))
        return login.ruoli.includes("Admin")
    }

    function isLogged() {
        return login != null && login.token != "" && login.token != null && login.token != undefined
    }

    const handleLogout = () => {
        dispatch(resetToken())
        navigateTo("/")
    }

    const handleSignUp = () => {
        navigateTo("/registration")
    }

    const handleLogin = () => {
        dispatch(resetToken())
        navigateTo("/login")
    }

    return (
        <>
            <nav className={"navbar navbar-expand-lg navbar-light " + css.myNav}>
                <div className="container-fluid">
                    <div className={"navbar-brand " + css.myBrand}>LearnXperience</div>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ms-auto">
                            {!isLogged() ?
                                <>
                                    <li className="nav-item px-1 my-1 my-lg-0">
                                        <button name="signup" className="btn btn-success" onClick={handleSignUp}>Sign Up</button>
                                    </li>
                                    <li className="nav-item px-1 my-1 my-lg-0">
                                        <button name="login" className="btn btn-success" onClick={handleLogin}>Log In</button>
                                    </li>
                                </>
                                :
                                <>
                                    <li className="nav-item px-1 my-1 my-lg-0">
                                        {
                                            isAdmin() ?
                                                <NavLink
                                                    to="/users"
                                                    style={({ isActive }) => ({
                                                        color: isActive
                                                            ? "navy"
                                                            : "black",
                                                    })}>Users</NavLink>
                                                :
                                                <></>
                                        }
                                    </li>
                                    <li className="nav-item px-1 my-1 my-lg-0">
                                        <button name="logout" className="btn btn-danger" onClick={handleLogout}>Log Out</button>
                                    </li>
                                </>}
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    )
}