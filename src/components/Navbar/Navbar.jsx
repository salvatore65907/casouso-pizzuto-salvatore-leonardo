//////// NON PIU' UTILIZZATA ////////


import { NavLink } from "react-router-dom"
import { useNavigate } from "react-router-dom"
import { useContext } from "react"
import css from './Navbar.module.css'
import { AuthContext } from "../../contexts/AuthContext"

export default function Navbar() {

    const [user, setUser] = useContext(AuthContext)

    //Per navigare
    const navigateTo = useNavigate()

    const handleClick = (e) => {
        const {name} = e.target
        if(name == "login") {
            navigateTo("/login")
        } else if(name == "registration") {
            navigateTo("/registration")
        } else if(name == "logout") {
            navigateTo("/")
        }
    }

    return (
        <>
            <nav className={"navbar sticky-top navbar-expand-lg " + css.myNav}>
                <div className="container-fluid">
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                        <a className="navbar-brand" href="#">Hidden brand</a>
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            {}
                            <li className="nav-item">
                                {user.ruoli.includes("Admin") ?
                                    <NavLink className="nav-link active" aria-current="page" to="users">All Users</NavLink>
                                    :
                                    <></>
                                }
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Link</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link disabled" aria-disabled="true">Disabled</a>
                            </li>
                        </ul>
                        {/*<form className="d-flex" role="search">
                            <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                                <button className="btn btn-outline-success" type="submit">Search</button>
                            </form>*/}
                        <div className="d-flex">
                            {!user.logged ?
                                <>
                                    <div className="me-2">
                                        <button name="login" className={"btn btn-success me-2 "} onClick={handleClick}>Log In</button>
                                    </div>
                                    <div>
                                        <button name="registration" className={"btn btn-success me-2 "} onClick={handleClick}>Sign Up</button>
                                    </div>
                                </>
                                :
                                <>
                                <div>
                                    <button name="logout" className="btn btn-danger" onClick={handleClick}>Log Out</button>
                                </div>
                                </>
                            }
                        </div>
                    </div>
                </div>
            </nav>
        </>
    )
}