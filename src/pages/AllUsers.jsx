import ListUsers from "../components/ListUsers/ListUsers"

export default function AllUsers() {
    return (
        <div className="container text-center my-4">
            <h1>Users</h1>
            <ListUsers />
        </div>
    )
} 