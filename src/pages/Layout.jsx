import { useOutlet } from "react-router-dom";
import Navbar from "../components/Navbar/Navbar";
import Navbar2 from "../components/Navbar/Navbar2";
import Footer from "../components/Footer/Footer";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { resetToken } from '../Store/Reducers/Login'

export default function Layout() {
    const outlet = useOutlet()

    const[users, setUsers] = useState([])

    let token = useSelector((state) => {
        return state.login.token
    })

    let dispatch = useDispatch()
    
    useEffect(() => {
       // dispatch(resetToken())
    }, [])

    return (
        <>
                <Navbar2 />
                <div className={""}>
                    {outlet} {/*Raccoglie le rotte a cui applicare il template*/}
                </div>
                <Footer />
        </>
    )
}