import { combineReducers } from '@reduxjs/toolkit';
import { loginSlice } from './Login';
import { utentiSlice } from './Users';

export default combineReducers({
    login: loginSlice.reducer,
    utenti: utentiSlice.reducer
});
