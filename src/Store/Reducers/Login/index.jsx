import { createSlice } from "@reduxjs/toolkit";
import { jwtDecode } from "jwt-decode";


const loginState={
    token: "",
    nome: "",
    cognome: "",
    email: "",
    ruoli: []    
};

export const loginSlice=createSlice({
    name: "token",
    initialState: loginState,
    reducers:{
        setToken: (state, action) => {
            state.token = action.payload
            const decoded = jwtDecode(action.payload)
            state.nome = decoded.nome
            state.cognome = decoded.cognome
            state.email = decoded.email
            state.ruoli = decoded.ruoli
        },
        resetToken: (state) => {
            state.token = "",
            state.nome = "",
            state.cognome = "",
            state.email = "",
            state.ruoli = []
        }
    }
})

export const {setToken, resetToken } = loginSlice.actions;
