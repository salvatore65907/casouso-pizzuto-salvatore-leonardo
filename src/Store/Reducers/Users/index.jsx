import { createSlice } from "@reduxjs/toolkit";


const utentiState={
    lista: null
};

export const utentiSlice=createSlice({
    name: "utenti",
    initialState: utentiState,
    reducers:{
        setUtenti: (state, action) => {
            state.lista = action.payload
        }
    }
})

export const {setUtenti} = utentiSlice.actions;
